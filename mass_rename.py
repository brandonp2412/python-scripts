import os
import re
import glob


def clean(string):
    '''Enforce my naming convention for files'''
    clean_string = re.sub('[- ]', '_', string)
    clean_string = re.sub('[^0-9A-Za-z_./]', '', clean_string)
    return clean_string


def rename_file(name):
    new_file_name = clean(file_name)
    print(f'Renaming file {file_name} to {new_file_name}')
    if file_name == new_file_name:
        return
    try:
        os.rename(file_name, new_file_name)
    except FileNotFoundError:
        pass


def get_files():
    return glob.glob("**/*", recursive=True)


files = get_files()
while any(re.search(pattern="[^0-9A-Za-z_./]", string=f) for f in files):
    for file_name in files:
        rename_file(file_name)
    files = get_files()
