# Mass Rename
`mass_rename.py` cleans the names of all files and subdirectories in the current
directory. For example, the file name:
```
Hello, World!.txt
```

Would be renamed to:
```
Hello_World.txt
```

## Usage

```shell
$ python3 mass_rename.py
Renaming file Hello, World!.txt to Hello_World.txt
```

# Convert to MP4

`convert_to_mp4.py` takes a directory of videos and converts each file to .mp4.

## Dependencies

- [ffmpeg](https://github.com/kkroening/ffmpeg-python#installation)

## Usage

```shell
$ python3 convert_to_mp4.py
...
```