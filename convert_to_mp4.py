import glob
import os
import re
import ffmpeg
from video_extensions import video_extensions

videos = []
for extension in video_extensions:
    videos.extend(glob.glob(f'**/*.{extension}', recursive=True))

for video in videos:
    stream = ffmpeg.input(video)
    name, extension = os.path.splitext(video)
    stream = ffmpeg.output(stream, name + '.mp4')
    ffmpeg.run(stream)
    os.remove(video)